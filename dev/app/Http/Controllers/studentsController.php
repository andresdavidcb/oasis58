<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\student;
use App\Models\id_type;
use App\Models\course_category;
use App\Models\person;
use App\Models\interest_course;

use Mail;
use Session;
use Redirect;

class studentsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        
        $students=student::name()->get();
        $id_types=id_type::all();
        $course_categories=course_category::all();
        $people=person::all();
        
       return [
            'students'=>$students,
            'id_types'=>$id_types,
            'course_categories'=>$course_categories,
            'people'=>$people
        ];
    }
}
