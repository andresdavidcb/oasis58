<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table="students";
    protected $fillable = [
        'interest_courses','person','created_at'
    ];

    public function scopeName($query){
    	/*$query->select('students.id','person','people.lastname as lastname','id_types.name as id_type','id_types.id as id_type_key','people.identification as identification',
        'people.name as name','people.main_phone as main_phone','people.sec_phone as sec_phone','people.email as email','students.interest_courses','students.created_at as created_date','enrolment_statuses.name as status','people.observations')
        ->join('people','people.id','=','students.person')
        ->join('enrolment_statuses','enrolment_statuses.id','=','students.enrolment_status')
        ->join('id_types','id_types.id','=','people.id_type');*/
        $query->select('students.id','person','people.name as name','people.lastname as lastname',
        'people.main_phone as main_phone','people.sec_phone as sec_phone','students.interest_courses',
        'id_types.name as id_type','enrolment_statuses.name as status')
        ->join('people','people.id','=','students.person')
        ->join('id_types','id_types.id','=','people.id_type')
        ->join('enrolment_statuses','enrolment_statuses.id','=','students.enrolment_status')
        ;

    }
    
    public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function enrolments()
    {
        return $this->hasMany(enrolment::class);
    }

	public function id_type()
	{
		return $this->belongsTo(id_type::class);
	}

    public function interest_courses()
    {
        return $this->hasMany(interest_course::class);
    }
}
