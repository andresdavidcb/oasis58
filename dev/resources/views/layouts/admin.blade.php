<?php $url= $_SERVER['REQUEST_URI'];
$thislink=explode('/',$url);
$a= $thislink[1];
$active="active";
?>

<div class="btn-toolbar" role="toolbar" aria-label="...">
  <div class="btn-group btn-group btn-group-lg" role="group" aria-label="..."><a href="{{ url('students') }}" class=" list-group-item {{$a=='students'?$active:''}}">Estudiantes</a></div>
  <div class="btn-group btn-group btn-group-lg" role="group" aria-label="..."><a href="{{ url('teachers') }}" class=" list-group-item {{$a=='teachers'?$active:''}}">Profesores</a></div>
  <div class="btn-group btn-group btn-group-lg" role="group" aria-label="..."><a href="{{ url('assesors') }}" class=" list-group-item {{$a=='assesors'?$active:''}}">Asesores</a></div>
</div>
<!--
<div class="list-group col-md-2">
    
  <a href="{{ url('studentslist') }}" class="list-group-item {{$a=='studentslist'?$active:''}}">Estudiantes</a>	
  <a href="{{ url('teacherslist') }}" class="list-group-item {{$a=='teacherslist'?$active:''}}">Profesores</a>	
  <a href="{{ url('assesorslist') }}" class="list-group-item {{$a=='assesorslist'?$active:''}}">Asesores</a>	
  <a href="{{ url('courses_shedulelist') }}" class="list-group-item {{$a=='courses_shedulelist'?$active:''}}">Cursos</a>	
</div>
-->