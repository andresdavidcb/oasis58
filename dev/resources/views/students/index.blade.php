@extends('layouts.app')
@section('content')
@include('layouts.admin')



<div id="main" class="container col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class='table'>
                      <thead><tr>
                      <th>Acciones</th>
                      <th>Nombre completo</th>
                      <th>Teléfono principal</th>
                      <th>Cursos de interes</th>  
                      <th>Estado</th>  
                      </tr></thead>
                      <tbody>
                          <tr v-for="item in list.students">
                          <td>
                          
                          </td>
                        <td>@{{item.name}} @{{item.lastname}}</td>
                        <td>@{{item.main_phone}}</td>
                        <td>@{{item.interest_courses}}</td>
                        <td>@{{item.status}}</td>
                        
                          </tr>
                          
                      </tbody>
                  
                    </table>
    </div>
</div><!--endPannel-->

            



        </div>
    </div>


    <pre>
        @{{$data}}
    </pre>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>

<script>
new Vue({
  el: '#main',
  data () {
    return {
      url:'liststudents',
      list: null
    }
  },
  mounted () {
    axios
      //.get('https://api.coindesk.com/v1/bpi/currentprice.json')
      .get(this.url)
      .then(response => (this.list = response.data))
  }
})
</script>
@endsection
